import { Component, OnInit } from "@angular/core";
import { HeroeModel } from "src/app/models/heroe.model";
import { NgForm } from "@angular/forms";
import { HeroesService } from "src/app/services/heroes.service";
import Swal from "sweetalert2";
import { Observable } from "rxjs";
@Component({
  selector: "app-heroe",
  templateUrl: "./heroe.component.html",
  styleUrls: ["./heroe.component.css"]
})
export class HeroeComponent implements OnInit {
  heroe: HeroeModel = new HeroeModel();
  constructor(private heroesService: HeroesService) {}

  ngOnInit() {}

  guardar(form: NgForm) {
    let edit: boolean = false;
    if (!form.valid) {
      console.log("El formulario no es valido");
      return;
    }

    Swal.fire({
      title: "Espera",
      text: "Guardando Informacion",
      icon: "info",
      allowOutsideClick: false
    });
    Swal.showLoading();

    let peticion: Observable<any>;

    if (this.heroe.id) {
      edit = true;
      peticion = this.heroesService.actualizarHeroe(this.heroe);
    } else {
      peticion = this.heroesService.crearHeroe(this.heroe);
    }

    peticion.subscribe(resp => {
      Swal.fire({
        title: this.heroe.nombre,
        text: edit
          ? "Se actualizo correctamente"
          : "Heroe guardado correctamente",
        icon: "success"
      });
    });
  }
}
